﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using blog.DAL;

namespace blog.Controllers
{
    public class PostController : Controller
    {

        public IActionResult Index()
        {
            BlogContext context = HttpContext.RequestServices.GetService(typeof(blog.DAL.BlogContext)) as BlogContext;

            return View(context.GetAllPosts());
        }

    }
}
