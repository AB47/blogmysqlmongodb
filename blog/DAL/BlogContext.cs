﻿using blog.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace blog.DAL
{
    public class BlogContext
    {
        public string ConnectionString { get; set; }

        public BlogContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public List<Post> GetAllPosts()
        {
            List<Post> list = new List<Post>();

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select * from Post where id < 100", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Post()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            Title = reader["Title"].ToString(),
                            Text = reader["Text"].ToString(),
                            Author = reader["Author"].ToString()
                        });
                    }
                }
            }
            return list;
        }

    }
}
